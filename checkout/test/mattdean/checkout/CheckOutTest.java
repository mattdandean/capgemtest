package mattdean.checkout;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import mattdean.checkout.discount.Discount;
import mattdean.checkout.discount.ThreeForTwoDiscount;
import mattdean.checkout.discount.TwoForOneDiscount;

public class CheckOutTest {
	
	CheckOut checkOut; 
	HashMap<String,Double> priceList = new HashMap<String,Double>();
	ArrayList<Discount> discountList = new ArrayList<Discount>();
	
	@Before
	public void setup(){
		checkOut = new CheckOut(); 
		priceList.put("Apple", 0.60);
		priceList.put("Orange", 0.25);		
		checkOut.setPriceList(priceList);
		discountList.add(new TwoForOneDiscount("Apple"));
		discountList.add(new ThreeForTwoDiscount("Orange"));
		checkOut.setDiscountList(discountList);
	}
	
	@Test
	public void testCheckoutEmptyCart(){
		ArrayList<String> cart = new ArrayList<String>(); 
		assertEquals(Double.valueOf(0.0),checkOut.checkOut(cart));
	}
	
	@Test
	public void testCheckoutUnrecongisedItem(){
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Lemon");
		assertEquals(Double.valueOf(0.0),checkOut.checkOut(cart));
	}
	
	@Test
	public void testCheckoutApple(){
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Apple");
		assertEquals(Double.valueOf(0.60),checkOut.checkOut(cart));
	}
	
	@Test
	public void testCheckoutOrange(){
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Orange");
		assertEquals(Double.valueOf(0.25),checkOut.checkOut(cart));
	}
	
	@Test
	public void testCheckoutAppleAndOranges(){
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Orange");
		cart.add("Apple");
		assertEquals(Double.valueOf(0.85),checkOut.checkOut(cart));
	}
	
	@Test 
	public void testCheckOutAppleSpecialOffer(){
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Apple");
		cart.add("Apple");
		assertEquals(Double.valueOf(0.60),checkOut.checkOut(cart));
	}

	
	@Test 
	public void testCheckOutOrangeSpecialOffer(){
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Orange");
		cart.add("Orange");
		cart.add("Orange");
		assertEquals(Double.valueOf(0.50),checkOut.checkOut(cart));
	}
	

	@Test 
	public void testCheckOutOrangeandApplesSpecialOffer(){
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Apple");
		cart.add("Apple");		 
		cart.add("Orange");
		cart.add("Orange");
		cart.add("Orange");
		assertEquals(Double.valueOf(1.10),checkOut.checkOut(cart));
	}
	
}
