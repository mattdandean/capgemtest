package mattdean.checkout.discount;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

public class ThreeForTwoDiscountTest {

	@Test
	public void testDiscount(){	 
		HashMap<String,Double> priceList = new HashMap<String,Double>();
		priceList.put("Apple", 0.60);
		priceList.put("Orange", 0.25);
		ArrayList<String> cart = new ArrayList<String>(); 
		cart.add("Orange");
		cart.add("Orange");
		cart.add("Orange");
		ThreeForTwoDiscount discount = new ThreeForTwoDiscount("Orange"); 
		assertEquals(Double.valueOf(0.25),discount.calculateDiscountForCart(cart, priceList));		
	}
	
}
