package mattdean.checkout.discount;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class TwoForOneDiscount implements Discount {

	String product;

	/*
	 * @param product to apply discount on
	 */
	public TwoForOneDiscount(String product) {
		this.product = product;
	}

	@Override
	public Double calculateDiscountForCart(List<String> cart, Map<String, Double> priceList) {

		int numApples = Collections.frequency(cart, product);

		return Math.floor(numApples / 2) * priceList.get(product);
		
	}

}
