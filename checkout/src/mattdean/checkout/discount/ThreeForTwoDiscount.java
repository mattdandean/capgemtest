package mattdean.checkout.discount;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ThreeForTwoDiscount implements Discount {

	String product;

	public ThreeForTwoDiscount(String product){
		this.product = product;
	}
		
	@Override
	public Double calculateDiscountForCart(List<String> cart, Map<String, Double> priceList) {
		
		int numApples = Collections.frequency(cart, product);
		
		return Math.floor(numApples/3) * priceList.get(product);
		
	}
	
	
	
}
