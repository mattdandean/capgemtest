package mattdean.checkout.discount;

import java.util.List;
import java.util.Map;

public interface Discount {
	
	public Double calculateDiscountForCart(List<String> cart, Map<String,Double> priceList);
		
}
