package mattdean.checkout;

import java.util.List;
import java.util.Map;

import mattdean.checkout.discount.Discount;

public class CheckOut {

	private Map<String, Double> priceList;

	private List<Discount> discountList;

	/*
	 * Returns total price of a cart of items according to set Price and Discount Lists
	 */
	public Double checkOut(List<String> cart) {
			
		Double total = 0.0;
		
		if(priceList == null ) return total;

		for (String item : cart) {

			Double price = priceList.get(item);

			if (price != null) {
				total += price;
			}

		}

		if(discountList == null) return total;
		
		double discountValue = 0.0;

		for (Discount discount : discountList) {
			discountValue += discount.calculateDiscountForCart(cart, priceList);
		}

		return (total - discountValue);
	}

	public List<Discount> getDiscountList() {
		return discountList;
	}

	public void setDiscountList(List<Discount> discountList) {
		this.discountList = discountList;
	}

	public Map<String, Double> getPriceList() {
		return priceList;
	}

	public void setPriceList(Map<String, Double> priceList) {
		this.priceList = priceList;
	}

}
